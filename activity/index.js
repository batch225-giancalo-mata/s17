/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printWelcomeMessages() {
	let fullName = prompt("Enter Full Name");
	let userAge = prompt("Enter Age");
	let userLocation = prompt("Enter Location")

	console.log("Full Name: " + fullName)
	console.log("Age: " + userAge)
	console.log("Location: " + userLocation )

}

printWelcomeMessages();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function printArtists() {
	console.log("Usher");
	console.log("Ja Rule");
	console.log("Eminem");
	console.log("Snoop Dogg");
	console.log("2pac");
}

printArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function printMovies() {
	console.log("1. Avengers Endgame: 94%");
	console.log("2. Avengers Infinity War: 85%");
	console.log("3. Avengers Age of Ultron: 76%");
	console.log("4. Iron Man 1: 94%");
	console.log("5. Iron Man 2: 71%");
}

printMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1)
	console.log(friend2)
	console.log(friend3)
}

printFriends();